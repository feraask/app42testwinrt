﻿using com.shephertz.app42.paas.sdk.windows;
using com.shephertz.app42.paas.sdk.windows.storage;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace App42TestWinRT
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
		public const string apiKey = "REPLACE_WITH_API_KEY";
		public const string secretKey = "REPLACE_WITH_SECRET_KEY";
		public const string dbName = "TESTUSERS";
		public const string collName = "Users";
		public static ServiceAPI app42Api = new ServiceAPI(MainPage.apiKey, MainPage.secretKey);
		public static StorageService storageService = MainPage.app42Api.BuildStorageService();
		public static MainPage current;
		public static Windows.UI.Core.CoreDispatcher dispatcher;

		public ObservableCollection<string> documentList { get; set; }

		public static async Task SetStatusBar(bool isEnabled, string text = "loading", double indicatorMode = -1, int clearTime = 0)
		{
#if WINDOWS_PHONE_APP
			await MainPage.dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, async () =>
			{
				var statusBar = Windows.UI.ViewManagement.StatusBar.GetForCurrentView();
				if (isEnabled)
				{
					statusBar.ProgressIndicator.Text = text;
					if (indicatorMode == -1)
						statusBar.ProgressIndicator.ProgressValue = null;
					else
						statusBar.ProgressIndicator.ProgressValue = indicatorMode;
					await statusBar.ProgressIndicator.ShowAsync();
					if (clearTime > 0)
					{
						await Task.Delay(clearTime);
						await statusBar.ProgressIndicator.HideAsync();
						statusBar.ProgressIndicator.Text = "";
					}
				}
				else
				{
					await statusBar.ProgressIndicator.HideAsync();
					statusBar.ProgressIndicator.Text = "";
					statusBar.ProgressIndicator.ProgressValue = null;
				}
			});
#endif
		}

        public MainPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;

			this.documentList = new ObservableCollection<string>();
			MainPage.dispatcher = Window.Current.Dispatcher;
			MainPage.current = this;
			this.DataContext = this;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
			// Using this next line it does not work.
			string[] usernames = { "player1", "player2", "player3", "player4", "player5", "player6_username", "player7_username", "the last player username" };

			// Using this line it works
			//string[] usernames = { "player1", "player2", "player3", "player4", "player5", "player6_username", "player7_username"};

			List<Query> queries = new List<Query>();
			Query mainQuery;

			foreach (string name in usernames)
			{
				queries.Add(QueryBuilder.Build("username", name, Operator.EQUALS));
			}

			mainQuery = QueryBuilder.CompoundOperator(queries[0], Operator.OR, queries[1]);
			for (int i = 2; i < queries.Count; i++)
			{
				mainQuery = QueryBuilder.CompoundOperator(mainQuery, Operator.OR, queries[i]);
			}

			Task status = MainPage.SetStatusBar(true, "getting documents...");
			storageService.FindDocumentByQuery(MainPage.dbName, MainPage.collName, mainQuery, new BaseApp42Callback());
            // TODO: Prepare page for display here.

            // TODO: If your application contains multiple pages, ensure that you are
            // handling the hardware Back button by registering for the
            // Windows.Phone.UI.Input.HardwareButtons.BackPressed event.
            // If you are using the NavigationHelper provided by some templates,
            // this event is handled for you.
        }
    }

	public class BaseApp42Callback : App42Callback
	{
		public void OnException(App42Exception exception)
		{
			Debug.WriteLine(exception.GetMessage());
			Task status = MainPage.SetStatusBar(true, "error fetching documents", 0, 5000);
		}

		public void OnSuccess(object response)
		{
			Task loadDocs = MainPage.dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
				{
					var result = (Storage)response;
					IList<Storage.JSONDocument> docs = result.GetJsonDocList();
					foreach (var document in docs)
					{
						string jsonDoc = document.GetJsonDoc();
						MainPage.current.documentList.Add(jsonDoc);
					}
					Task status = MainPage.SetStatusBar(true, "documents fetched", 0, 5000);
				}).AsTask();
		}
	}
}
